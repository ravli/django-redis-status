This little application just shows current Redis stats on Django's admin dashboard. Also supports django-admin-tools nicely.

Moved to [https://github.com/ilvar/django-redis-stats-instant](https://github.com/ilvar/django-redis-stats-instant)
